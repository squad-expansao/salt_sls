call_execution_function:
  salt.function:
    - tgt: '*'
    - name: cmd.run
    - arg:
      - date

call_state_functions:
  salt.state:
    - tgt: '*'
    - sls:
      - apache.welcome
